#include <catch.hpp>

#include "vector2.hpp"

// compile template functions
typedef Vector2<float> Vector2f;

// NOTE: static variables tested
// [-] Vector2::down
// [-] Vector2::left
// [-] Vector2::one
// [-] Vector2::right
// [-] Vector2::up
// [-] Vector2::zero

// NOTE: static functions tested
// [-] Vector2::angle
// [-] Vector2::distance
// [-] Vector2::dot
// [-] Vector2::length
// [-] Vector2::max
// [-] Vector2::min
// [ ] Vector2::normalize

namespace test {

TEST_CASE("static variables", "[vector2]") {
	SECTION("shorthand for writing Vector2(0, -1).") {
		auto v = Vector2f::down;
		REQUIRE(v.x == 0.0f);
		REQUIRE(v.y == -1.0f);
	}
	SECTION("shorthand for writing Vector2(-1, 0).") {
		auto v = Vector2f::left;
		REQUIRE(v.x == -1.0f);
		REQUIRE(v.y == 0.0f);
	}
	SECTION("shorthand for writing Vector2(1, 1).") {
		auto v = Vector2f::one;
		REQUIRE(v.x == 1.0f);
		REQUIRE(v.y == 1.0f);
	}
	SECTION("shorthand for writing Vector2(1, 0).") {
		auto v = Vector2f::right;
		REQUIRE(v.x == 1.0f);
		REQUIRE(v.y == 0.0f);
	}
	SECTION("shorthand for writing Vector2(0, 1).") {
		auto v = Vector2f::up;
		REQUIRE(v.x == 0.0f);
		REQUIRE(v.y == 1.0f);
	}
	SECTION("shorthand for writing Vector2(0, 0).") {
		auto v = Vector2f::zero;
		REQUIRE(v.x == 0.0f);
		REQUIRE(v.y == 0.0f);
	}
}

TEST_CASE("vectors can be constructed without arguments", "[vector2]") {
	auto v = Vector2f();
	SECTION("resizing bigger changes size and capacity") {
		REQUIRE(v.x == 0.0f);
		REQUIRE(v.y == 0.0f);
	}
}

TEST_CASE("calculating the angle between two vectors", "[vector2]") {
	auto a = Vector2f(1.0f, 2.0f);
	auto b = Vector2f(3.0f, 4.0f);

	auto angle = static_cast<float>(std::acos(
		((a.x * b.x) + (a.y * b.y)) / (std::sqrt((a.x * a.x) + (a.y * a.y))) * (std::sqrt((b.x * b.x) + (b.y * b.y)))
	));

	REQUIRE(Vector2f::angle(a, b) == angle);
	REQUIRE(Vector2f::angle(a, b) == 10.3048f);
}

TEST_CASE("calculating the distance between two vectors", "[vector2]") {
	auto a = Vector2f(1.0f, 2.0f);
	auto b = Vector2f(3.0f, 4.0f);

	auto distance = static_cast<float>(std::sqrt(((b[0] - a[0]) * (b[0] - a[0])) + ((b[1] - a[1]) * (b[1] - a[1]))));

	REQUIRE(Vector2f::distance(a, b) == distance);
	REQUIRE(Vector2f::distance(a, b) == (2.0f * std::sqrtf(2.0f)));
}

TEST_CASE("calculating the dot product of two vectors", "[vector2]") {
	auto a = Vector2f(1.0f, 2.0f);
	auto b = Vector2f(3.0f, 4.0f);

	// The dot product can also be defined as the sum of the products of the components of each vector.
	auto dot = ((a[0] * b[0]) + (a[1] * b[1]));

	REQUIRE(Vector2f::dot(a, b) == dot);
	REQUIRE(Vector2f::dot(a, b) == 11.0f);
}

TEST_CASE("calculating the length (magnitude) of a vector", "[vector2]") {
	auto a = Vector2f(1.0f, 2.0f);
	auto b = Vector2f(3.0f, 4.0f);

	// The dot product can also be defined as the sum of the products of the components of each vector.
	auto len_a = static_cast<float>(std::sqrt((a[0] * a[0]) + (a[1] * a[1])));
	auto len_b = static_cast<float>(std::sqrt((b[0] * b[0]) + (b[1] * b[1])));

	REQUIRE(Vector2f::length(a) == len_a);
	REQUIRE(Vector2f::length(a) == std::sqrtf(5.0f));

	REQUIRE(Vector2f::length(b) == len_b);
	REQUIRE(Vector2f::length(b) == std::sqrtf(25.0f));
}

TEST_CASE("calculating MAX vector", "[vector2]") {
	auto a = Vector2f(3.0f, 2.0f);
	auto b = Vector2f(1.0f, 4.0f);
	auto m = Vector2f::max(a, b);

	REQUIRE(m[0] == 3.0f);
	REQUIRE(m[1] == 4.0f);
}

TEST_CASE("calculating MIN vector", "[vector2]") {
	auto a = Vector2f(3.0f, 2.0f);
	auto b = Vector2f(1.0f, 4.0f);
	auto m = Vector2f::max(a, b);

	REQUIRE(m[0] == 2.0f);
	REQUIRE(m[1] == 1.0f);
}


TEST_CASE("vectors can be normalized", "[vector2]") {
	auto a = Vector2f(1.0f, 2.0f);
	auto b = Vector2f(3.0f, 4.0f);

	// The dot product can also be defined as the sum of the products of the components of each vector.
	/*auto norm_a = ((a[0] * b[0]) + (a[1] * b[1]));*/

	REQUIRE(a.normalized().length() == 1.0f);
	REQUIRE(Vector2f::normalize(a).length() == 1.0f);

	a.normalize();
	REQUIRE(a.length() == 1.0f);
}

TEST_CASE("vectors have operators", "[vector2]") {
	SECTION("element access `[]`") {
		auto v = Vector2f(1.0f, 2.0f);
		REQUIRE(v.x == v[0]);
		REQUIRE(v.y == v[1]);

		v[0] = 3.0f;
		v[1] = 4.0f;

		REQUIRE(v.x == 3.0f);
		REQUIRE(v.y == 4.0f);
	}
}

TEST_CASE("vector components", "[vector2]") {
	SECTION("(x, y) components") {
		auto v = Vector2f(1.0f, 2.0f);

		// (x, y) coordinates
		REQUIRE(v.x == 1.0f);
		REQUIRE(v.y == 2.0f);
	}

	SECTION("(u, v) components") {
		auto v = Vector2f(1.0f, 2.0f);

		// (u, v) coordinates
		REQUIRE(v.u == 1.0f);
		REQUIRE(v.v == 2.0f);
	}
}

}  // test
