#pragma once

// STL
#include <cmath>

// forward declaration
template <typename T> class Vector2;
template <typename T> class Vector3;

template <typename T>
class Vector4 {
// type definitions
protected:
	using Vector2 = Vector2<T>;
	using Vector3 = Vector3<T>;
	
// public variables
public:
	union { T x; };
	union { T y; };
	union { T z; };
	union { T w; };

// constructors
public:
	Vector4();
	Vector4(T x, T y, T z, T w);
	Vector4(const Vector4 &v);

	template<class _T>
	Vector4(const Vector4<_T> &v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
		, z(static_cast<T>(v.z))
		, w(static_cast<T>(v.w)) {
	}

	~Vector4() = default;

// operators
public:
	T & operator [] (int i);
	const T &operator [] (int i) const;

	Vector4 &operator = (const Vector4 &v);

	Vector4 operator + (const Vector4 &v) const;
	Vector4& operator += (const Vector4 &v);

	Vector4 operator - (const Vector4 &v) const;
	Vector4& operator -= (const Vector4 &v);

	Vector4 operator * (const Vector4 &v) const;
	Vector4& operator *= (const Vector4 &v);

	Vector4 operator / (const Vector4 &v) const;
	Vector4& operator /= (const Vector4 &v);

	Vector4 operator * (const T &s) const;
	Vector4& operator *= (const T &s);

	Vector4 operator / (const T &s) const;
	Vector4& operator /= (const T &s);

	Vector4 operator - () const;

	operator Vector2() const {
		return Vector2{
			this->x,
			this->y,
		};
	}

	operator Vector3() const {
		return Vector3{
			this->x,
			this->y,
			this->z,
		};
	}

// public functions
public:
	T magnitude() const;
	void normalize();
	Vector4 normalized() const;
	T sqrMagnitude() const;

// static functions
public:
	static T angle(const Vector4 &a, const Vector4 &b);
	static T distance(const Vector4 &a, const Vector4 &b);
	static T dot(const Vector4 &a, const Vector4 &b);
	static T magnitude(const Vector4 &v);
	static Vector4 max(const Vector4 &a, const Vector4 &b);
	static Vector4 min(const Vector4 &a, const Vector4 &b);
	static Vector4 normalize(const Vector4 &v);
	static T sqrMagnitude(const Vector4 &v);
};

template <typename T>
inline Vector4<T>::Vector4()
	: x{ 0 }
	, y{ 0 }
	, z{ 0 }
	, w{ 0 } {

}

template <typename T>
inline Vector4<T>::Vector4(T x, T y, T z, T w)
	: x{ x }
	, y{ y }
	, z{ z }
	, w{ w } {

}

template <typename T>
inline Vector4<T>::Vector4(const Vector4<T> &v)
	: x{ v.x }
	, y{ v.y }
	, z{ v.z }
	, w{ v.w } {

}

template <typename T>
inline T &Vector4<T>::operator [] (int i) {
	switch (i) {
	default:
	case 0:
		return this->x;
	case 1:
		return this->y;
	case 2:
		return this->z;
	case 3:
		return this->w;
	}
}

template <typename T>
inline const T &Vector4<T>::operator [] (int i) const {
	switch (i) {
	default:
	case 0:
		return this->x;
	case 1:
		return this->y;
	case 2:
		return this->z;
	case 3:
		return this->w;
	}
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator = (const Vector4<T> &v) {
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
	this->w = v.w;
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator + (const Vector4<T> &v) const {
	return Vector4<T>{
		this->x + v.x,
		this->y + v.y,
		this->z + v.z,
		this->w + v.w,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator += (const Vector4<T> &v) {
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	this->w += v.w;
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator - (const Vector4<T> &v) const {
	return Vector4<T>{
		this->x - v.x,
		this->y - v.y,
		this->z - v.z,
		this->w - v.w,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator -= (const Vector4<T> &v) {
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	this->w -= v.w;
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator * (const Vector4<T> &v) const {
	return Vector4<T>{
		this->x * v.x,
		this->y * v.y,
		this->z * v.z,
		this->w * v.w,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator *= (const Vector4<T> &v) {
	this->x *= v.x;
	this->y *= v.y;
	this->z *= v.z;
	this->w *= v.w;
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator / (const Vector4<T> &v) const {
	return Vector4<T>{
		this->x / v.x,
		this->y / v.y,
		this->z / v.z,
		this->w / v.w,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator /= (const Vector4<T> &v) {
	this->x /= v.x;
	this->y /= v.y;
	this->z /= v.z;
	this->w /= v.w;
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator * (const T &s) const {
	return Vector4<T>{
		this->x * s,
		this->y * s,
		this->z * s,
		this->w * s,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator *= (const T &s) {
	this->x *= static_cast<T>(s);
	this->y *= static_cast<T>(s);
	this->z *= static_cast<T>(s);
	this->w *= static_cast<T>(s);
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator / (const T &s) const {
	return Vector4<T>{
		this->x / s,
		this->y / s,
		this->z / s,
		this->w / s,
	};
}

template <typename T>
inline Vector4<T> &Vector4<T>::operator /= (const T &s) {
	this->x /= static_cast<T>(s);
	this->y /= static_cast<T>(s);
	this->z /= static_cast<T>(s);
	this->w /= static_cast<T>(s);
	return (*this);
}

template <typename T>
inline Vector4<T> Vector4<T>::operator - () const {
	return Vector4<T>{
		-this->x,
		-this->y,
		-this->z,
		-this->w
	};
}

template <typename T>
inline T Vector4<T>::magnitude() const {
	return static_cast<T>(std::sqrt((x * x) + (y * y) + (z * z) + (w * w)));
}

template <typename T>
inline void Vector4<T>::normalize() {
	T m = magnitude();
	this->x /= m;
	this->y /= m;
	this->z /= m;
	this->w /= m;
}

template <typename T>
inline Vector4<T> Vector4<T>::normalized() const {
	Vector4<T> v{ *this };
	v.normalize();
	return v;
}

template <typename T>
inline T Vector4<T>::sqrMagnitude() const {
	return static_cast<T>((x * x) + (y * y) + (z * z) + (w * w));
}

template <typename T>
inline T Vector4<T>::angle(const Vector4<T> &a, const Vector4<T> &b) {
	return (std::acos(dot(a, b) / (magnitude(a) * magnitude(b))) / 0.0174533f);
}

template <typename T>
inline T Vector4<T>::distance(const Vector4<T> &a, const Vector4<T> &b) {
	return static_cast<T>(magnitude(b - a));
}

template <typename T>
inline T Vector4<T>::dot(const Vector4<T> &a, const Vector4<T> &b) {
	return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w));
}

template <typename T>
inline T Vector4<T>::magnitude(const Vector4<T> &v) {
	return static_cast<T>(std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w)));
}

template <typename T>
inline Vector4<T> Vector4<T>::max(const Vector4<T> &a, const Vector4<T> &b) {
	return Vector4<T>{
		std::max(a.x, b.x),
		std::max(a.y, b.y),
		std::max(a.z, b.z),
		std::max(a.w, b.w),
	};
}

template <typename T>
inline Vector4<T> Vector4<T>::min(const Vector4<T> &a, const Vector4<T> &b) {
	return Vector4<T>{
		std::min(a.x, b.x),
		std::min(a.y, b.y),
		std::min(a.z, b.z),
		std::min(a.w, b.w),
	};
}

template <typename T>
inline Vector4<T> Vector4<T>::normalize(const Vector4<T> &v) {
	return Vector4<T>{
		v.normalized()
	};
}

template <typename T>
inline T Vector4<T>::sqrMagnitude(const Vector4<T> &v) {
	return static_cast<T>((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w));
}
