#pragma once

// STL
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <algorithm>

// forward declaration
template <typename T> class Vector2;
template <typename T> class Vector4;

template <typename T>
class Vector3 {
// type definitions
protected:
	using Vector2 = Vector2<T>;
	using Vector4 = Vector4<T>;

// public variables
public:
	union { T x; };
	union { T y; };
	union { T z; };

// constructors
public:
	Vector3();
	Vector3(T x, T y, T z);
	Vector3(const Vector3 &v);

	template<class _T>
	Vector3(const Vector3<_T> &v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
		, z(static_cast<T>(v.z)) {

	}

	~Vector3() = default;

// operators
public:
	T &operator [] (int i);
	const T &operator [] (int i) const;

	Vector3 &operator = (const Vector3 &v);

	Vector3 operator + (const Vector3 &v) const;
	Vector3& operator += (const Vector3 &v);

	Vector3 operator - (const Vector3 &v) const;
	Vector3& operator -= (const Vector3 &v);

	Vector3 operator * (const Vector3 &v) const;
	Vector3& operator *= (const Vector3 &v);

	Vector3 operator / (const Vector3 &v) const;
	Vector3& operator /= (const Vector3 &v);

	Vector3 operator * (const T &s) const;
	Vector3& operator *= (const T &s);

	Vector3 operator / (const T &s) const;
	Vector3& operator /= (const T &s);

	Vector3 operator - () const;

	operator Vector2() const {
		return Vector2{
			this->x,
			this->y,
		};
	}

	operator Vector4() const {
		return Vector4{
			this->x,
			this->y,
			this->z,
			static_cast<T>(0)
		};
	}

// public functions
public:
	T magnitude() const;
	void normalize();
	Vector3 normalized() const;
	T sqrMagnitude() const;

// static functions
public:
	static T angle(const Vector3 &a, const Vector3 &b);
	static T distance(const Vector3 &a, const Vector3 &b);
	static T dot(const Vector3 &a, const Vector3 &b);
	static T magnitude(const Vector3 &v);
	static Vector3 max(const Vector3 &a, const Vector3 &b);
	static Vector3 min(const Vector3 &a, const Vector3 &b);
	static Vector3 normalize(const Vector3 &v);
	static T sqrMagnitude(const Vector3 &v);
};

template <typename T>
inline Vector3<T>::Vector3()
	: x{ 0 }
	, y{ 0 }
	, z{ 0 } {

}

template <typename T>
inline Vector3<T>::Vector3(T x, T y, T z)
	: x{ x }
	, y{ y }
	, z{ z } {

}

template <typename T>
inline Vector3<T>::Vector3(const Vector3<T> &v)
	: x{ v.x }
	, y{ v.y }
	, z{ v.z } {

}

template <typename T>
inline T &Vector3<T>::operator [] (int i) {
	switch (i) {
	default:
	case 0:
		return this->x;
	case 1:
		return this->y;
	case 2:
		return this->z;
	}
}

template <typename T>
inline const T &Vector3<T>::operator [] (int i) const {
	switch (i) {
	default:
	case 0:
		return this->x;
	case 1:
		return this->y;
	case 2:
		return this->z;
	}
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator = (const Vector3<T> &v) {
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator + (const Vector3<T> &v) const {
	return Vector3<T>{
		this->x + v.x,
		this->y + v.y,
		this->z + v.z,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator += (const Vector3<T> &v) {
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator - (const Vector3<T> &v) const {
	return Vector3<T>{
		this->x - v.x,
		this->y - v.y,
		this->z - v.z,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator -= (const Vector3<T> &v) {
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator * (const Vector3<T> &v) const {
	return Vector3<T>{
		this->x * v.x,
		this->y * v.y,
		this->z * v.z,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator *= (const Vector3<T> &v) {
	this->x *= v.x;
	this->y *= v.y;
	this->z *= v.z;
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator / (const Vector3<T> &v) const {
	return Vector3<T>{
		this->x / v.x,
		this->y / v.y,
		this->z / v.z,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator /= (const Vector3<T> &v) {
	this->x /= v.x;
	this->y /= v.y;
	this->z /= v.z;
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator * (const T &s) const {
	return Vector3<T>{
		this->x * s,
		this->y * s,
		this->z * s,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator *= (const T &s) {
	this->x *= static_cast<T>(s);
	this->y *= static_cast<T>(s);
	this->z *= static_cast<T>(s);
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator / (const T &s) const {
	return Vector3<T>{
		this->x / s,
		this->y / s,
		this->z / s,
	};
}

template <typename T>
inline Vector3<T> &Vector3<T>::operator /= (const T &s) {
	this->x /= static_cast<T>(s);
	this->y /= static_cast<T>(s);
	this->z /= static_cast<T>(s);
	return (*this);
}

template <typename T>
inline Vector3<T> Vector3<T>::operator - () const {
	return Vector3<T>{
		-this->x,
		-this->y,
		-this->z
	};
}

template <typename T>
inline T Vector3<T>::magnitude() const {
	return static_cast<T>(std::sqrt((x * x) + (y * y) + (z * z)));
}

template <typename T>
inline void Vector3<T>::normalize() {
	T m = magnitude();
	this->x /= m;
	this->y /= m;
	this->z /= m;
}

template <typename T>
inline Vector3<T> Vector3<T>::normalized() const {
	Vector3<T> v{ *this };
	v.normalize();
	return v;
}

template <typename T>
inline T Vector3<T>::sqrMagnitude() const {
	return static_cast<T>((x * x) + (y * y) + (z * z));
}

template <typename T>
inline T Vector3<T>::angle(const Vector3<T> &a, const Vector3<T> &b) {
	return static_cast<T>(std::acos(dot(a, b)));
}

template <typename T>
inline T Vector3<T>::distance(const Vector3<T> &a, const Vector3<T> &b) {
	return static_cast<T>(magnitude(b - a));
}

template <typename T>
inline T Vector3<T>::dot(const Vector3<T> &a, const Vector3<T> &b) {
	return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
}

template <typename T>
inline T Vector3<T>::magnitude(const Vector3<T> &v) {
	return static_cast<T>(std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
}

template <typename T>
inline Vector3<T> Vector3<T>::max(const Vector3<T> &a, const Vector3<T> &b) {
	return Vector3<T>{
		std::max(a.x, b.x),
		std::max(a.y, b.y),
		std::max(a.z, b.z),
	};
}

template <typename T>
inline Vector3<T> Vector3<T>::min(const Vector3<T> &a, const Vector3<T> &b) {
	return Vector3<T>{
		std::min(a.x, b.x),
		std::min(a.y, b.y),
		std::min(a.z, b.z),
	};
}

template <typename T>
inline Vector3<T> Vector3<T>::normalize(const Vector3<T> &v) {
	return Vector3<T>{
		v.normalized()
	};
}

template <typename T>
inline T Vector3<T>::sqrMagnitude(const Vector3<T> &v) {
	return static_cast<T>(v.x * v.x + v.y * v.y + v.z * v.z);
}
