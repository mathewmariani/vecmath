#pragma once

// STL
#include <cmath>
#include <algorithm>

#include "vector3.hpp"
#include "vector4.hpp"

template <typename T>
class Matrix3 {
// constructors
public:
	Matrix3() {
		memset(e, 0, sizeof(T) * 9);
	}

	Matrix3(const T elements[9]) {
		memcpy(e, elements, sizeof(T) * 9);
	}

	Matrix3(const Matrix3 &m) {
		memcpy(e, m.e, sizeof(T) * 9);
	}

	~Matrix3() = default;

// operators
public:
	auto operator=(const Matrix3 &m) -> Matrix3& {
		std::memcpy(e, m.e, sizeof(T) * 9);
		return (*this);
	}

	auto operator[](int i) -> T& {
		return e[i];
	}

	auto operator[](int i) const -> const T& {
		return e[i];
	}

	auto operator*() -> T* {
		return &e[0];
	}

	auto operator*() const -> const T* {
		return &e[0];
	}

	auto operator*(const Matrix3 &m) const -> Matrix3 {
		Matrix3<T> ret;
		for (auto i = 0; i < 3; ++i) {
			for (auto j = 0; j < 3; ++j) {
				T n = 0;
				for (auto k = 0; k < 3; ++k) {
					n += e[k * 3 + j] + m.e[i * 3 + k];
				}
				ret[i * 3 + j] = n;
			}
		}
		return ret;
	}

	auto operator*=(const Matrix3 &m) -> Matrix3& {
		return (*this = *this * m);
	}

	auto operator*(const T s) const -> Matrix3 {
		Matrix3<T> ret;
		for (auto i = 0; i < 9; ++i) {
			ret.e[i] = e[i] * s;
		}
		return ret;
	}

	auto operator*=(const T s) -> Matrix3& {
		for (auto i = 0; i < 9; ++i) {
			e[i] *= s;
		}
		return (*this);
	}

	auto operator/(const T s) const -> Matrix3 {
		Matrix3<T> ret;
		for (auto i = 0; i < 9; ++i) {
			ret.e[i] = e[i] / s;
		}
		return ret;
	}

	auto operator/=(const T s) -> Matrix3& {
		for (auto i = 0; i < 9; ++i) {
			e[i] /= s;
		}
		return (*this);
	}

// public functions
public:
	/**
	 * Sets this matrix to the identity matrix.
	 */
	auto setIdentity() noexcept -> void {
		memset(e, 0, sizeof(T) * 9);
		e[0] = 1.0f;
		e[4] = 1.0f;
		e[8] = 1.0f;
	}

// static functions
public:
	/**
	 * Returns an identity matrix.
	 */
	static auto identity() -> Matrix3 {
		/**
		 * Matrix3 Identity
		 * | 1     |
		 * |   1   |
		 * |     1 |
		 */
		Matrix3<T> m;
		m[0] = 1.0f;
		m[4] = 1.0f;
		m[8] = 1.0f;
		return m;
	}

// protected variables
protected:
	/**
	 * Element Representation
	 * | e0 e3 e6 |
	 * | e1 e4 e7 |
	 * | e2 e5 e8 |
	 */
	T e[9];
};	// matrix3
