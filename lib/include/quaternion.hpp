#pragma once

// STL
#include <cmath>

#include "matrix4.hpp"
#include "vector3.hpp"
#include "vector4.hpp"

template <typename T>
class Quaternion {
// public variables
public:
	T w;
	Vector3<T> v;

// static variables
public:
	static const Quaternion identity;

// constructors
public:
	Quaternion();
	Quaternion(T w, T x, T y, T z);
	Quaternion(T w, const Vector3<T> &v);
	Quaternion(const Quaternion &q);

// operators
public:
	Quaternion& operator = (const Quaternion &q);

	Quaternion operator ~ () const;
	Quaternion operator - () const;

	Quaternion operator + (const Quaternion &q) const;
	Quaternion& operator += (const Quaternion &q);

	Quaternion operator - (const Quaternion &q) const;
	Quaternion& operator -= (const Quaternion &q);

	Quaternion operator * (const Quaternion &q) const;
	Quaternion& operator *= (const Quaternion &q);

	Quaternion operator * (const T s) const;
	Quaternion& operator *= (const T s);

	Quaternion operator / (const T s) const;
	Quaternion& operator /= (const T s);

// public functions
public:
	T length() const;
	void normalize();
	Quaternion normalized() const;

	Vector3<T> rotatePoint(const Quaternion &q) const;
	Vector3<T> rotatePoint(const Vector3<T> &p) const;

	Matrix4<T> transform() const;

// static functions
public:
	static Quaternion angleAxis(float angle, Vector3<T> axis);
	static Quaternion angleAxis(float angle, T x, T y, T z);
};

template <typename T>
const Quaternion<T> Quaternion<T>::identity{ 1, 0, 0, 0 };

template <typename T>
inline Quaternion<T>::Quaternion()
	: w{ 0 }
	, v{ 0, 0, 0 } {

}

template <typename T>
inline Quaternion<T>::Quaternion(T w, T x, T y, T z)
	: w{ w }
	, v{ x, y, z } {

}

template <typename T>
inline Quaternion<T>::Quaternion(T w, const Vector3<T> &v)
	: w{ w }
	, v{ v } {

}

template <typename T>
inline Quaternion<T>::Quaternion(const Quaternion<T> &q)
	: w{ q.w }
	, v{ q.v } {

}

template <typename T>
inline Quaternion<T>& Quaternion<T>::operator = (const Quaternion<T> &q) {
	this->w = q.w;
	this->v = q.v;
	return (*this);
}

template <typename T>
inline Quaternion<T> Quaternion<T>::operator ~ () const {
	return Quaternion<T>{
		this->w,
		-this->v,
	};
}

template <typename T>
inline Quaternion<T> Quaternion<T>::operator - () const {
	return Quaternion<T>{
		-this->w,
		-this->v,
	};
}

template <typename T>
inline Quaternion<T> Quaternion<T>::operator + (const Quaternion<T> &q) const {
	return Quaternion<T>{
		this->w + q.w,
		this->v + q.v
	};
}

template <typename T>
inline Quaternion<T> &Quaternion<T>::operator += (const Quaternion<T> &q) {
	this->w += q.w;
	this->v += q.v;
	return (*this);
}

template <typename T>
inline Quaternion<T> Quaternion<T>::operator - (const Quaternion<T> &q) const {
	return Quaternion<T>{
		this->w - q.w,
		this->v - q.v
	};
}

template <typename T>
inline Quaternion<T> &Quaternion<T>::operator -= (const Quaternion<T> &q) {
	this->w -= q.w;
	this->v -= q.v;
	return (*this);
}

template <typename T>
inline Quaternion<T> Quaternion<T>::operator * (const Quaternion<T> &q) const {
	//const auto& lhs = *this;
	//const auto& rhs = q;
	//return Quaternion<T>{
	//	lhs.w * rhs.w - lhs.v.x * rhs.v.x - lhs.v.y * rhs.v.y - lhs.v.z * rhs.v.z,
	//	lhs.w * rhs.v.x + lhs.v.x * rhs.w + lhs.v.y * rhs.v.z - lhs.v.z * rhs.v.y,
	//	lhs.w * rhs.v.y - lhs.v.x * rhs.v.z + lhs.v.y * rhs.w + lhs.v.z * rhs.v.x,
	//	lhs.w * rhs.v.z + lhs.v.x * rhs.v.y - lhs.v.y * rhs.v.x + lhs.v.z * rhs.w
	//};

	const auto& lhs = *this;
	const auto& rhs = q;

	auto w = lhs.w * rhs.w - lhs.v.x * rhs.v.x - lhs.v.y * rhs.v.y - lhs.v.z * rhs.v.z;
	auto x = lhs.w * rhs.v.x + lhs.v.x * rhs.w + lhs.v.y * rhs.v.z - lhs.v.z * rhs.v.y;
	auto y = lhs.w * rhs.v.y - lhs.v.x * rhs.v.z + lhs.v.y * rhs.w + lhs.v.z * rhs.v.x;
	auto z = lhs.w * rhs.v.z + lhs.v.x * rhs.v.y - lhs.v.y * rhs.v.x + lhs.v.z * rhs.w;

	return Quaternion<T>{ w, x, y, z };
}

template <typename T>
inline Quaternion<T> &Quaternion<T>::operator *= (const Quaternion<T> &q) {
	Quaternion _q{ (*this) * q };
	v = _q.v;
	w = _q.w;
	return (*this);
}

template <typename T>
Quaternion<T> Quaternion<T>::operator * (const T s) const {
	return Quaternion<T>{
		this->w * s,
		this->v * s
	};
}

template <typename T>
Quaternion<T> &Quaternion<T>::operator *= (const T s) {
	this->w *= s;
	this->v *= s;
	return (*this);
}

template <typename T>
Quaternion<T> Quaternion<T>::operator / (const T s) const {
	return Quaternion<T>{
		this->w / s,
		this->v / s
	};
}

template <typename T>
Quaternion<T> &Quaternion<T>::operator /= (const T s) {
	this->w /= s;
	this->v /= s;
	return (*this);
}

template <typename T>
inline T Quaternion<T>::length() const {
	return static_cast<T>(std::sqrtf(w * w + v.sqrMagnitude()));
}

template <typename T>
inline void Quaternion<T>::normalize() {
	auto len = length();
	this->w /= len;
	this->v /= len;
}

template <typename T>
inline Quaternion<T> Quaternion<T>::normalized() const {
	Quaternion<T> q{ w, v };
	q.normalize();
	return q;
}

template <typename T>
inline Vector3<T> Quaternion<T>::rotatePoint(const Quaternion<T> &q) const {
	auto &rotation = *this;
	auto result = rotation * q * ~rotation;
	return result.v;
}

template <typename T>
inline Vector3<T> Quaternion<T>::rotatePoint(const Vector3<T> &p) const {
	return rotatePoint(Quaternion<T>{ 0, p });
}

template <typename T>
inline Matrix4<T> Quaternion<T>::transform() const {
	Matrix4<T> ret;

	auto xx = v.x * v.x;
	auto xy = v.x * v.y;
	auto xz = v.x * v.z;
	auto xw = v.x * w;

	auto yy = v.y * v.y;
	auto yz = v.y * v.z;
	auto yw = v.y * w;

	auto zz = v.z * v.z;
	auto zw = v.z * w;

	ret[0] = 1 - 2 * (yy + zz);
	ret[4] = 2 * (xy - zw);
	ret[8] = 2 * (xz + yw);
	ret[12] = 0;

	ret[1] = 2 * (xy + zw);
	ret[5] = 1 - 2 * (xx + zz);
	ret[9] = 2 * (yz - xw);
	ret[13] = 0;

	ret[2] = 2 * (xz - yw);
	ret[6] = 2 * (yz + xw);
	ret[10] = 1 - 2 * (xx + yy);
	ret[14] = 0;

	ret[3] = 0;
	ret[7] = 0;
	ret[11] = 0;
	ret[15] = 1;

	return ret;
}

#define DEG2RAD(x) ((x * M_PI) / 180.0)

template <typename T>
inline Quaternion<T> Quaternion<T>::angleAxis(float a, Vector3<T> axis) {
	axis.normalize();
	const auto rads = float(a) * 0.0174532925f;
	const auto c = std::cosf(rads / 2.0f);
	const auto s = std::sinf(rads / 2.0f);
	return Quaternion<T>{ c, axis * s };
}

template <typename T>
inline Quaternion<T> Quaternion<T>::angleAxis(float deg, T x, T y, T z) {
	return Quaternion<T>::angleAxis(deg, Vector3<T>{ x, y, z });
}
