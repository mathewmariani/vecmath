#pragma once

// STL
#include <cmath>
#include <algorithm>

// forward declaration
template <typename T> class Quaternion;
template <typename T> class Vector3;
template <typename T> class Vector4;

template <typename T>
class Matrix4 {
// type definitions
protected:
	using Quaternion = Quaternion<T>;
	using Vector3 = Vector3<T>;
	using Vector4 = Vector4<T>;

// constructors
public:
	Matrix4() {
		memset(e, 0, sizeof(T) * 16);
	}

	Matrix4(const T elements[16]) {
		memcpy(e, elements, sizeof(T) * 16);
	}

	Matrix4(const Matrix4 &m) {
		memcpy(e, m.e, sizeof(T) * 16);
	}

	~Matrix4() = default;

// operators
public:
	auto operator=(const Matrix4 &m) -> Matrix4& {
		std::memcpy(e, m.e, sizeof(T) * 16);
		return (*this);
	}

	auto operator[](int i) -> T& {
		return e[i];
	}

	auto operator[](int i) const -> const T& {
		return e[i];
	}

	auto operator*() -> T* {
		return &e[0];
	}

	auto operator*() const -> const T* {
		return &e[0];
	}

	auto operator*(const Matrix4 &m) const -> Matrix4 {
		Matrix4<T> ret;
		for (auto i = 0; i < 4; ++i) {
			for (auto j = 0; j < 4; ++j) {
				T n = 0;
				for (auto k = 0; k < 4; ++k) {
					n += e[k * 4 + j] + m.e[i * 4 + k];
				}
				ret[i * 4 + j] = n;
			}
		}
		return ret;
	}

	auto operator*=(const Matrix4 &m) -> Matrix4& {
		return (*this = *this * m);
	}

	auto operator*(const T s) const -> Matrix4 {
		Matrix4<T> ret;
		for (auto i = 0; i < 16; ++i) {
			ret.e[i] = e[i] * s;
		}
		return ret;
	}

	auto operator*=(const T s) -> Matrix4& {
		for (auto i = 0; i < 16; ++i) {
			e[i] *= s;
		}
		return (*this);
	}

	auto operator/(const T s) const -> Matrix4 {
		Matrix4<T> ret;
		for (auto i = 0; i < 16; ++i) {
			ret.e[i] = e[i] / s;
		}
		return ret;
	}

	auto operator/=(const T s) -> Matrix4& {
		for (auto i = 0; i < 16; ++i) {
			e[i] /= s;
		}
		return (*this);
	}

	auto operator*(const Vector3 &v) const -> Vector3 {
		const auto& rhs = v;
		return Vector3{
			e[0] * rhs.x + e[4] * rhs.y + e[8] * rhs.z,
			e[1] * rhs.x + e[5] * rhs.y + e[9] * rhs.z,
			e[2] * rhs.x + e[6] * rhs.y + e[10] * rhs.z
		};
	}

	auto operator*(const Vector4 &v) const -> Vector4 {
		const auto& rhs = v;
		return Vector4{
			e[0] * rhs.x + e[4] * rhs.y + e[8] * rhs.z + e[12] * rhs.w,
			e[1] * rhs.x + e[5] * rhs.y + e[9] * rhs.z + e[13] * rhs.w,
			e[2] * rhs.x + e[6] * rhs.y + e[10] * rhs.z + e[14] * rhs.w,
			e[3] * rhs.x + e[7] * rhs.y + e[11] * rhs.z + e[15] * rhs.w
		};
	}

// public functions
public:
	/**
	 * Sets this matrix to the identity matrix.
	 */
	auto setIdentity() -> void {
		memset(e, 0, sizeof(T) * 16);
		e[0] = 1.0f;
		e[5] = 1.0f;
		e[10] = 1.0f;
		e[15] = 1.0f;
	}

// static functions
public:
	/**
	 * Returns an indentity matrix.
	 */
	static auto identity() -> Matrix4 {
		/**
		 * Matrix4 Identity
		 * | 1       |
		 * |   1     |
		 * |     1   |
		 * |       1 |
		 */
		Matrix4<T> m;
		m[0] = 1.0f;
		m[5] = 1.0f;
		m[10] = 1.0f;
		m[15] = 1.0f;
		return m;
	}

	/**
	 * Returns a translation matrix.
	 * @param  x [description]
	 * @param  y [description]
	 * @param  z [description]
	 * @param  w [description]
	 */
	static auto translate(T x, T y, T z, T w = 1) -> Matrix4 {
		/**
		 * Matrix4 Translation
		 * | 1       |
		 * |   1     |
		 * |     1   |
		 * | x y z w |
		 */
		auto m = Matrix4::identity();
		m[12] = x;
		m[13] = y;
		m[14] = z;
		m[15] = w;
		return m;
	}

	/**
	 * Returns a scaling matrix.
	 * @param  s [description]
	 */
	static auto scale(T s) -> Matrix4 {
		/**
		 * Matrix4 Scale
		 * | sx      |
		 * |   sy    |
		 * |     sz  |
		 * |       1 |
		 */
		auto ret = Matrix4::identity();
		ret[0] = s;
		ret[5] = s;
		ret[10] = s;
		return ret;
	}

	/**
	 * Returns a rotation matrix.
	 * @param  a [description]
	 * @param  x [description]
	 * @param  y [description]
	 * @param  z [description]
	 */
	static auto rotate(T a, T x, T y, T z) -> Matrix4 {
		const auto rads = float(a) * 0.0174532925f;
		const auto c = std::cosf(rads);
		const auto s = std::sinf(rads);
		const auto ux = (x * x);
		const auto uy = (y * y);
		const auto uz = (z * z);

		auto ret = Matrix4::identity();
		ret[0] = (c + ux * (1 - c));
		ret[1] = ((y * x) * (1 - c) + z * s);
		ret[2] = ((z * x) * (1 - c) - y * s);
		ret[4] = ((x * y) * (1 - c) - z * s);
		ret[5] = (c + uy * (1 - c));
		ret[6] = ((z * y) * (1 - c) + x * s);
		ret[8] = ((x * z) * (1 - c) + y * s);
		ret[9] = ((y * z) * (1 - c) - x * s);
		ret[10] = (c + uz * (1 - c));
		return ret;
	}

	static auto rotate(Quaternion q) -> void {
		// TODO: ...
	}

	/**
	 * Returns a rotation matrix about the x-axis.
	 * @param  a [description]
	 */
	static auto rotateX(T a) -> Matrix4 {
		/**
		 * Matrix4 Rotation X
		 * | 1       |
		 * |   c s   |
		 * |  -s c   |
		 * |       1 |
		 */
		const auto rads = float(a) * 0.0174532925f;
		const auto c = std::cosf(rads);
		const auto s = std::sinf(rads);

		auto ret = Matrix4::identity();
		ret[5] = (c);
		ret[6] = (s);
		ret[9] = (-s);
		ret[10] = (c);
		return ret;
	}

	/**
	 * Returns a rotation matrix about the y-axis.
	 * @param  a [description]
	 */
	static auto rotateY(T a) -> Matrix4 {
		/**
		 * Matrix4 Rotation Y
		 * | c  -s   |
		 * |   1     |
		 * | s   c   |
		 * |       1 |
		 */
		const auto rads = float(a) * 0.0174532925f;
		const auto c = std::cosf(rads);
		const auto s = std::sinf(rads);

		auto ret = Matrix4::identity();
		ret[0] = (c);
		ret[2] = (-s);
		ret[8] = (s);
		ret[10] = (c);
		return ret;
	}

	/**
	 * Returns a rotation matrix about the z-axis.
	 * @param  a [description]
	 */
	static auto rotateZ(T a) -> Matrix4 {
		/**
		 * Matrix4 Rotation Z
		 * | c s     |
		 * |-s c     |
		 * |         |
		 * |       1 |
		 */
		const auto rads = float(a) * 0.0174532925f;
		const auto c = std::cosf(rads);
		const auto s = std::sinf(rads);

		auto ret = Matrix4::identity();
		ret[0] = (c);
		ret[1] = (s);
		ret[4] = (-s);
		ret[5] = (c);
		return ret;
	}

	/**
	 * Returns a transformation matrix.
	 * @param  x     [description]
	 * @param  y     [description]
	 * @param  z     [description]
	 * @param  angle [description]
	 * @param  rx    [description]
	 * @param  ry    [description]
	 * @param  rz    [description]
	 * @param  sx    [description]
	 * @param  sy    [description]
	 * @param  sz    [description]
	 */
	static auto transform(T x, T y, T z, T a, T rx, T ry, T rz, T sx, T sy, T sz) -> Matrix4 {
		auto m = Matrix4::identity();

		const auto rads = float(a) * 0.0174532925f;
		const auto c = std::cosf(rads);
		const auto s = std::sinf(rads);
		const auto ux = (x * x);
		const auto uy = (y * y);
		const auto uz = (z * z);

		m[0] = sx * (c + ux * (1 - c));
		m[1] = sy * ((y * x) * (1 - c) + z * s);
		m[2] = sz * ((z * x) * (1 - c) - y * s);

		m[4] = sx * ((x * y) * (1 - c) - z * s);
		m[5] = sy * (c + uy * (1 - c));
		m[6] = sz * ((z * y) * (1 - c) + x * s);

		m[8] = sx * ((x * z) * (1 - c) + y * s);
		m[9] = sy * ((y * z) * (1 - c) - x * s);;
		m[10] = sz * (c + uz * (1 - c));

		m[12] = x * m[0] + y * m[4] + z * m[8];
		m[13] = x * m[1] + y * m[5] + z * m[9];
		m[14] = x * m[2] + y * m[6] + z * m[10];

		return m;
	}

	/**
	 * [transform description]
	 * @param  position [description]
	 * @param  q        [description]
	 * @param  scale    [description]
	 */
	static auto transform(Vector3 position, Quaternion q, Vector3 scale) -> void {
		// TODO: ...
	}

	/**
	 * Returns a perspective projection matrix.
	 * @param  fovy   [description]
	 * @param  aspect [description]
	 * @param  near   [description]
	 * @param  far    [description]
	 */
	static auto perspective(T fovy, T aspect, T near, T far) -> Matrix4 {
		auto ret = Matrix4::identity();
		float scale = (1.0f / std::tanf(fovy / 2.0f));
		float ratio = (scale / aspect);
		ret[0] = (ratio);
		ret[5] = (scale);
		ret[10] = (-(far) / (far - near));
		ret[11] = (-1.0f);
		ret[14] = (-(far * near) / (far - near));
		ret[15] = (0.0f);
		return ret;
	}

	/**
	 * Returns an orthogonal projection matrix.
	 * @param  left   [description]
	 * @param  right  [description]
	 * @param  bottom [description]
	 * @param  top    [description]
	 * @param  near   [description]
	 * @param  far    [description]
	 */
	static auto ortho(T left, T right, T bottom, T top, T near, T far) -> Matrix4 {
		auto ret = Matrix4::identity();
		ret[0] = (2.0f / (right - left));
		ret[5] = (2.0f / (top - bottom));
		ret[10] = (-2.0f / (far - near));
		ret[12] = (-(right + left) / (right - left));
		ret[13] = (-(top + bottom) / (top - bottom));
		ret[14] = (-(far + near) / (far - near));
		return ret;
	}

// protected variables
protected:
	/**
	 * Element Representation
	 * | e0 e4 e8  e12 |
	 * | e1 e5 e9  e13 |
	 * | e2 e6 e10 e14 |
	 * | e3 e7 e11 e15 |
	 */
	T e[16];
};	// matrix4
